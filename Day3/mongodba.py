import pprint
# inserting data in MongoDB
from datetime import datetime

from pymongo import MongoClient
#Checking whther connected to database or not
try:
	conn = MongoClient()
	print("Connected successfully!!!")
except:
	print("Could not connect to MongoDB")

# creating database
db = conn.PPAP_OEE

# Created or Switched to collection names: mydata
collection = db.production_orders

#passing a record
emp_rec1 = {
		"phone":8698569622,

		}



# Insert Data in table
rec_id1 = collection.insert_one(emp_rec1)


print("Data inserted with record ids",rec_id1)

# Printing the data inserted
cursor = collection.find()
for record in cursor:
	print(record)


#Data Updating

result = collection.update_many(
    {"eid": 24},
    {
        "$set": {
            "phone": 7020903373
        },
        "$currentDate": {"lastModified": True}

    }
)

print("Data updated with id", result)

# Print the new record
cursor = collection.find()
print("Printing new data...........")
for record in cursor:
    print(record)

#printing a record using find_one     ': ''
pprint.pprint(collection.find_one())

#printing the record which is eid greater than 14
gt=collection.find({"PART NUMBER": {"$gt": "79810T02T000"}})
print("printing greater than data.................")
for data in gt:
    print(data)
#printing the record which eid greater than and equal to14
gte=collection.find({"PART NUMBER": {"$gte": "79810T02T000"}})
print("printing greater than and equal to.................")
for get_data in gte:
    print(get_data)
#printing the record which eid less than 24
lt=collection.find({"PART NUMBER":{"$lt":"79810T02T000"}})
print("printing less than data..................")
for less_data in lt:
	print(less_data)
#printing the data which eid less than and equal to 24
lte=collection.find({"PART NUMBER":{"$lte":"79810T02T000"}})
print("printing less than and eqaul to data..................")
for eaual_data in lte:
	print(eaual_data)
#printing the record which eid is 24 and name is chandrakant
an=collection.find({"$and":[ {"PART NUMBER":"79810T02T000"}, {"NEW QTY":291}]})
print("Printing data for $and..................................")
for an1 in an:
	print(an1)
#Printing the data after 24 september
print("Printing data for after 24 september..................................")
start = datetime(2014, 9, 24, 00,00,00)
end=datetime(2020,9,24,00,00,00)
dt=collection.find({"ACTUAL_START":{"$gt":start}})
for dt1 in dt:
    print(dt1)

#printing the data between 23 and 24 sept
print("Printing data for between 23 september 24 sept..................................")
ed=collection.find(({"ACTUAL_START":{ "$gte": start, "$lt":end }}))

for dt2 in ed:
    print(dt1)
