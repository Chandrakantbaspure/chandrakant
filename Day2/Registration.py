
#importing required Packges

from random import randint
from time import strftime
from flask import Flask, render_template, flash, request,render_template
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField, form

DEBUG = True                                                    #Debug set True
app = Flask(__name__)                                           # creating the Flask class object
app.config.from_object(__name__)
app.config['SECRET_KEY'] = 'SjdnUends821Jsdlkvxh391ksdODnejdDw'   #used by Flask and extensions to keep data safe

class ReusableForm(Form):
    name = TextField('Name:', validators=[validators.required()])         #This is used for validation
    surname = TextField('Surname:', validators=[validators.required()])
    mobile = TextField('Mobile:', validators=[validators.required()])
    address= TextField('Address:', validators=[validators.required()])
    email = TextField('Email:', validators=[validators.required()])
    password = TextField('Password:', validators=[validators.required()])



def get_time():
    time = strftime("%Y-%m-%dT%H:%M")
    return time

#This is used for show file logs on Disl
def write_to_disk(name, surname, email):
    data = open('file.log', 'a')
    timestamp = get_time()
    data.write('DateStamp={}, Name={}, Surname={}, Email={} \n'.format(timestamp, name, surname, email))
    data.close()


@app.route("/", methods=['GET', 'POST'])        # decorator drfines the
def hello():
    form = ReusableForm(request.form)

    #print form errors
    if request.method == 'POST':
        name=request.form['name']
        surname=request.form['surname']
        email=request.form['email']
        password=request.form['password']
        mobile = request.form['mobile']
        address = request.form['address']

        if form.validate():
            write_to_disk(name, surname, email)
            flash('Hello: {} {}'.format(name, surname))   #if success then print name and surname as success

        else:
            flash('Error: All Fields are Required!!')      #else print error

    return render_template('Registration.html', form=form)   #render to html file

if __name__ == "__main__":
    app.run()