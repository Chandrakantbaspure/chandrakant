from flask import jsonify,Flask,request    #import packages
import json
app=Flask(__name__)
users=[{"Name":"Chandrakant"},{"Name":"Ram"}]     #create variables

@app.route('/',methods=['GET'])     #add decorater and GET method
def home():
    return jsonify({"output":"This is working"})      #returning first json format message
@app.route('/users',methods=['GET'])             # returnuing all Users
def GetUses():
    return jsonify({"users":users})

@app.route('/users',methods=['POST'])
def postUsers():
    #url='http://127.0.0.1:8000/getUsers'
    new_user_name=request.json['Name']    # create new varibale to store add new user
    new_user={'Name':new_user_name}

    users.append(new_user)               #append new user in users

    return jsonify({'Name':users})      #returning all users
if __name__ == '__main__':
	app.run(port=8000, debug=True)  #set port number 8000 and debug is True