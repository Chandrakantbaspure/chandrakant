from flask import * # here we are using * for importing all required packegs

app = Flask(__name__)  # creating the Flask class object


@app.route('/message')  # decorator drfines the
def message():
    return render_template('message.html'); # Here we are using rende_templte for redirect to message.html page and in that html page we wrote the message
if __name__ == '__main__':        # this is main function calling
    app.run(port=8000,debug=True) # Here this is using for by default debug mode is True and I gave port number is 8000 that means server will run on 8000 port